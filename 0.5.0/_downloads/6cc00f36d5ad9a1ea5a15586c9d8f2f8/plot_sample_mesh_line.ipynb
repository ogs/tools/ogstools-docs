{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Plot data of a sampling lines\n\n.. sectionauthor:: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nThis example provides clean coding recipes for plotting data of meshes over\nsampling lines. We also present different ways to setup the sampling lines.\nFor plotting we us the function :py:func:`ogstools.plot.line`.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from itertools import pairwise\n\nimport matplotlib.pyplot as plt\nimport numpy as np\nimport pyvista as pv\n\nimport ogstools as ot\nfrom ogstools import examples\n\not.plot.setup.show_region_bounds = False\nmesh = examples.load_mesh_mechanics_2D()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Simple case: straight line\nWe use the ``pyvista`` function ``sample_over_line`` and use two points to\ndefine the line and get a Mesh with the sampled data. Let's plot the Mesh and\nthe line together.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "sample = mesh.sample_over_line([25, -460, 0], [100, -800, 0])\nfig = mesh.plot_contourf(ot.variables.temperature)\nfig = ot.plot.line(sample, ax=fig.axes[0], linestyle=\"--\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Now we plot the temperature data. The spatial coordinate for the x-axis is\nautomatically detected here.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = ot.plot.line(sample, ot.variables.temperature)\nfig.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Simple case: circular arc\nWith 3 points we can define an arc over which to sample the data.\nHaving the arc directly on a boundary might result in some gaps in the\nsampled data, thus we extend the arc by a small margin.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "sample = mesh.sample_over_circular_arc(\n    pointa=[100 - 1e-4, -650, 0],\n    pointb=[200 + 1e-4, -650, 0],\n    center=[150, -650, 0],\n)\nfig, axs = plt.subplots(ncols=2, figsize=[26, 10])\nmesh.plot_contourf(ot.variables.displacement[\"x\"], fig=fig, ax=axs[1])\not.plot.line(sample, ax=axs[1], linewidth=\"8\", color=\"red\")\not.plot.line(sample, ot.variables.displacement[\"x\"], ax=axs[0])\nfig.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Other methods to setup the sampling line\nThe following section shows different methods of creating sampling lines.\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Linear spaced points\nThis basically does the same as the ``pyvista`` function `sample_over_line`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "pts = np.linspace([50, -460, 0], [50, -800, 0], 100)\nsample_1 = pv.PolyData(pts).sample(mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Mutilsegmented line from list of points\nThe following code allows you to have a line connecting multiple observation\npoints.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "obs_pts = np.asarray([[150, -460, 0], [50, -650, 0], [150, -800, 0]])\npts = np.vstack([np.linspace(pt1, pt2, 50) for pt1, pt2 in pairwise(obs_pts)])\nsample_2 = pv.PolyData(pts).sample(mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Spline from list of points\nYou can also create smooth sampling lines by using a fitting function.\nThe following creates a second order polynomial fit for the x-coordinates\nin dependence of the y-coordinates.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "pts = np.asarray([[200, -460, 0], [250, -650, 0], [200, -800, 0]])\nfit = np.poly1d(np.polyfit(pts[:, 1], pts[:, 0], 2))\ny = np.linspace(-460, -800, 100)\npts = np.transpose([fit(y), y, y * 0])\nsample_3 = pv.PolyData(pts).sample(mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Use existing geometry\nAnother way to setup the sampling line is to extract points from the domain\nmesh. Here, we use the ``clip`` function from ``pyvista`` and some boolean logic,\nto extract a vertical line through the center, which follows the boundary of\nthe hole. We need to sort the points however, to have them adjacent.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "edges = mesh.clip(\"x\").extract_feature_edges()\nis_top_bot = np.isin(edges.points[:, 1], [-800, -460])\nis_left = edges.points[:, 0] == 0\npts = edges.points[np.invert(is_top_bot | is_left)]\nsample_4 = pv.PolyData(pts[np.argsort(pts[:, 1])]).sample(mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Now we plot all samples for comparison.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig, axs = plt.subplots(ncols=2, figsize=[26, 10])\nu_x = ot.variables.displacement[\"x\"]\nmesh.plot_contourf(u_x, fig=fig, ax=axs[1])\nfor i, sample in enumerate([sample_1, sample_2, sample_3, sample_4]):\n    c = f\"C{i}\"  # cycle through default color cycle\n    ot.plot.line(sample, ax=axs[1], linestyle=\"--\", color=c)\n    ot.plot.line(sample, u_x, \"y\", ax=axs[0], label=f\"sample {i + 1}\", color=c)\nfig.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "If you want to sample data over multiple timesteps in a MeshSeries, have a\nlook at `sphx_glr_auto_examples_howto_plot_plot_timeslice.py`.\n\n"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}