{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Stress analysis\n\n.. sectionauthor:: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nThe following example from the ogs benchmark collection is used for the\nstress analysis:\n\n<https://www.opengeosys.org/docs/benchmarks/thermo-mechanics/creepafterexcavation/>\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import ogstools as ot\nfrom ogstools import examples\n\nmesh = examples.load_mesh_mechanics_2D()\nfig = mesh.plot_contourf(ot.variables.displacement)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Tensor components\nWe can inspect the stress (or strain) tensor components by indexing.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(ot.variables.stress[\"xx\"])\nfig = mesh.plot_contourf(ot.variables.stress[\"xy\"])"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Principal stresses\nLet's plot the the principal stress components and also overlay the direction\nof the corresponding eigenvector in the plot. Note: the eigenvalues are sorted\nby increasing order, i.e. eigenvalue[0] is the most negative / largest\ncompressive principal stress.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "eigvecs = ot.variables.stress.eigenvectors\nfig = mesh.plot_contourf(variable=ot.variables.stress.eigenvalues[0])\nmesh.plot_quiver(ax=fig.axes[0], variable=eigvecs[0], glyph_type=\"line\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(variable=ot.variables.stress.eigenvalues[1])\nmesh.plot_quiver(ax=fig.axes[0], variable=eigvecs[1], glyph_type=\"line\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(variable=ot.variables.stress.eigenvalues[2])\nmesh.plot_quiver(ax=fig.axes[0], variable=eigvecs[2], glyph_type=\"line\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We can also plot the mean of the principal stress, i.e. the magnitude of the\nhydrostatic component of the stress tensor.\nsee: :py:func:`ogstools.variables.tensor_math.mean`\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(ot.variables.stress.mean)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Von Mises stress\nsee: :py:func:`ogstools.variables.tensor_math.von_mises`\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(ot.variables.stress.von_Mises)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## octahedral shear stress\nsee: :py:func:`ogstools.variables.tensor_math.octahedral_shear`\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(ot.variables.stress.octahedral_shear)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "# Integrity criteria\nEvaluating models regarding their integrity is often dependent on the\ngeometry, e.g. for a hypothetical water column proportional to the depth.\nPresets which fall under this category make use of\n:py:mod:`ogstools.variables.mesh_dependent`.\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The hypothetical water column used in the integrity criteria would initially\nuse existing \"pressure\" data in the mesh, otherwise it is automatically\ncalculated as the following:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh[\"pressure\"] = mesh.p_fluid()\nfig = mesh.plot_contourf(ot.variables.pressure)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "But since this assumes that the top of the model is equal to the ground\nsurface, the resulting pressure is underestimated. In this case we have to\ncorrect the depth manually. Then the pressure is calculated correctly:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh[\"depth\"] = mesh.depth(use_coords=True)\nfig = mesh.plot_contourf(\"depth\")\nmesh[\"pressure\"] = mesh.p_fluid()\nfig = mesh.plot_contourf(ot.variables.pressure)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Dilantancy criterion\nsee: :py:func:`ogstools.variables.mesh_dependent.dilatancy_critescu`\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(ot.variables.dilatancy_critescu_tot)\nfig = mesh.plot_contourf(ot.variables.dilatancy_critescu_eff)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Fluid pressure criterion\nsee: :py:func:`ogstools.variables.mesh_dependent.fluid_pressure_criterion`\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = mesh.plot_contourf(ot.variables.fluid_pressure_crit)"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}