{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Feflowlib: How to modify boundary conditions after conversion of a FEFLOW model.\n\n.. sectionauthor:: Julian Heinze (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nThis example shows how boundary conditions can be modified after converting a FEFLOW model.\nFirst we will change the values of the boundary conditions and later we will show how to define a new boundary mesh.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "0. Necessary imports\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import tempfile\nfrom pathlib import Path\n\nimport numpy as np\nimport pyvista as pv\n\nimport ogstools as ot\nfrom ogstools.examples import feflow_model_2D_HT\nfrom ogstools.feflowlib import assign_bulk_ids"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1. Load a FEFLOW model (.fem) as a FeflowModel object to further work it.\nDuring the initialisation, the FEFLOW file is converted.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "temp_dir = Path(tempfile.mkdtemp(\"converted_models\"))\nfeflow_model = ot.FeflowModel(feflow_model_2D_HT, temp_dir / \"HT_model\")\nmesh = feflow_model.mesh\n# Print information about the mesh.\nprint(mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "2. Plot the temperature simulated in FEFLOW on the mesh.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = ot.plot.contourf(mesh, \"P_TEMP\", show_edges=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "3. As the FEFLOW data now are a pyvista.UnstructuredGrid, all pyvista functionalities can be applied to it.\nFurther information can be found at https://docs.pyvista.org/version/stable/user-guide/simple.html.\nFor example it can be saved as a VTK Unstructured Grid File (\\*.vtu).\nThis allows to use the FEFLOW model for ``OGS`` simulation or to observe it in ``Paraview```.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "pv.save_meshio(temp_dir / \"HT_mesh.vtu\", mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "4. Run the FEFLOW model in OGS.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "feflow_model.setup_prj(end_time=1e11, time_stepping=[(1, 1e10)])\nfeflow_model.run()\nms = ot.MeshSeries(temp_dir / \"HT_model.pvd\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "5. Plot the temperature simulated in OGS.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "ogs_sim_res = ms.mesh(ms.timesteps[-1])\not.plot.contourf(ogs_sim_res, ot.variables.temperature)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6. The boundary meshes are manipulated to alter the model.\nThe original boundary conditions are shown in this example: `sphx_glr_auto_examples_howto_conversions_plot_F_feflowlib_HT_simulation.py`\n6.1 The Dirichlet boundary conditions for the hydraulic head are set to 0. Therefore, no water flows from the left to the right edge.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "bc_flow = feflow_model.subdomains[\"P_BC_FLOW\"][\"P_BC_FLOW\"]\nfeflow_model.subdomains[\"P_BC_FLOW\"][\"P_BC_FLOW\"][bc_flow == 10] = 0"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6.2 Overwrite the new boundary conditions and run the model.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "feflow_model.run(overwrite=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6.3 The corresponding simulation results look like.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "ms = ot.MeshSeries(temp_dir / \"HT_model.pvd\")\n# Read the last timestep:\nogs_sim_res = ms.mesh(ms.timesteps[-1])\not.plot.contourf(ogs_sim_res, ot.variables.temperature)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6.4 Create a new boundary mesh and overwrite the existing subdomains with this boundary mesh.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "assign_bulk_ids(mesh)\n# Get the points of the bulk mesh to build a new boundary mesh.\nwanted_pts = [1492, 1482, 1481, 1491, 1479, 1480, 1839, 1840]\nnew_bc = mesh.extract_points(\n    [pt in wanted_pts for pt in mesh[\"bulk_node_ids\"]],\n    adjacent_cells=False,\n    include_cells=False,\n)\nnew_bc[\"bulk_node_ids\"] = np.array(wanted_pts, dtype=np.uint64)\n# Define the temperature values of these points.\nnew_bc[\"P_BC_HEAT\"] = np.array([300] * len(wanted_pts), dtype=np.float64)\nfeflow_model.subdomains[\"P_BC_HEAT\"] = new_bc"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "7. Run the new model and plot the simulation results.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "feflow_model.run(overwrite=True)\nms = ot.MeshSeries(temp_dir / \"HT_model.pvd\")\nogs_sim_res = ms.mesh(ms.timesteps[-1])\not.plot.contourf(ogs_sim_res, ot.variables.temperature)"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}