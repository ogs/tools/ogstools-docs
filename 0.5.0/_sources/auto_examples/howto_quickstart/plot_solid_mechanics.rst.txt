
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "auto_examples/howto_quickstart/plot_solid_mechanics.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        :ref:`Go to the end <sphx_glr_download_auto_examples_howto_quickstart_plot_solid_mechanics.py>`
        to download the full example code.

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_auto_examples_howto_quickstart_plot_solid_mechanics.py:


Stress analysis
===============

.. sectionauthor:: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)

The following example from the ogs benchmark collection is used for the
stress analysis:

<https://www.opengeosys.org/docs/benchmarks/thermo-mechanics/creepafterexcavation/>

.. GENERATED FROM PYTHON SOURCE LINES 16-25

.. code-block:: Python




    import ogstools as ot
    from ogstools import examples

    mesh = examples.load_mesh_mechanics_2D()
    fig = mesh.plot_contourf(ot.variables.displacement)




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_001.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_001.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 31-34

Tensor components
-----------------
We can inspect the stress (or strain) tensor components by indexing.

.. GENERATED FROM PYTHON SOURCE LINES 36-39

.. code-block:: Python

    fig = mesh.plot_contourf(ot.variables.stress["xx"])
    fig = mesh.plot_contourf(ot.variables.stress["xy"])




.. rst-class:: sphx-glr-horizontal


    *

      .. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_002.png
         :alt: plot solid mechanics
         :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_002.png
         :class: sphx-glr-multi-img

    *

      .. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_003.png
         :alt: plot solid mechanics
         :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_003.png
         :class: sphx-glr-multi-img





.. GENERATED FROM PYTHON SOURCE LINES 40-46

Principal stresses
------------------
Let's plot the the principal stress components and also overlay the direction
of the corresponding eigenvector in the plot. Note: the eigenvalues are sorted
by increasing order, i.e. eigenvalue[0] is the most negative / largest
compressive principal stress.

.. GENERATED FROM PYTHON SOURCE LINES 48-52

.. code-block:: Python

    eigvecs = ot.variables.stress.eigenvectors
    fig = mesh.plot_contourf(variable=ot.variables.stress.eigenvalues[0])
    mesh.plot_quiver(ax=fig.axes[0], variable=eigvecs[0], glyph_type="line")




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_004.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_004.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 53-56

.. code-block:: Python

    fig = mesh.plot_contourf(variable=ot.variables.stress.eigenvalues[1])
    mesh.plot_quiver(ax=fig.axes[0], variable=eigvecs[1], glyph_type="line")




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_005.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_005.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 57-60

.. code-block:: Python

    fig = mesh.plot_contourf(variable=ot.variables.stress.eigenvalues[2])
    mesh.plot_quiver(ax=fig.axes[0], variable=eigvecs[2], glyph_type="line")




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_006.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_006.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 61-64

We can also plot the mean of the principal stress, i.e. the magnitude of the
hydrostatic component of the stress tensor.
see: :py:func:`ogstools.variables.tensor_math.mean`

.. GENERATED FROM PYTHON SOURCE LINES 66-68

.. code-block:: Python

    fig = mesh.plot_contourf(ot.variables.stress.mean)




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_007.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_007.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 69-72

Von Mises stress
----------------
see: :py:func:`ogstools.variables.tensor_math.von_mises`

.. GENERATED FROM PYTHON SOURCE LINES 74-76

.. code-block:: Python

    fig = mesh.plot_contourf(ot.variables.stress.von_Mises)




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_008.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_008.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 77-80

octahedral shear stress
-----------------------
see: :py:func:`ogstools.variables.tensor_math.octahedral_shear`

.. GENERATED FROM PYTHON SOURCE LINES 82-84

.. code-block:: Python

    fig = mesh.plot_contourf(ot.variables.stress.octahedral_shear)




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_009.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_009.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 85-91

Integrity criteria
==================
Evaluating models regarding their integrity is often dependent on the
geometry, e.g. for a hypothetical water column proportional to the depth.
Presets which fall under this category make use of
:py:mod:`ogstools.variables.mesh_dependent`.

.. GENERATED FROM PYTHON SOURCE LINES 93-96

The hypothetical water column used in the integrity criteria would initially
use existing "pressure" data in the mesh, otherwise it is automatically
calculated as the following:

.. GENERATED FROM PYTHON SOURCE LINES 98-101

.. code-block:: Python

    mesh["pressure"] = mesh.p_fluid()
    fig = mesh.plot_contourf(ot.variables.pressure)




.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_010.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_010.png
   :class: sphx-glr-single-img


.. rst-class:: sphx-glr-script-out

 .. code-block:: none

    /builds/ogs/tools/ogstools/.venv-devcontainer/lib/python3.10/site-packages/pyvista/core/dataset.py:2020: UnitStrippedWarning: The unit of the quantity is stripped when downcasting to ndarray.
      scalars = np.asanyarray(scalars)




.. GENERATED FROM PYTHON SOURCE LINES 102-105

But since this assumes that the top of the model is equal to the ground
surface, the resulting pressure is underestimated. In this case we have to
correct the depth manually. Then the pressure is calculated correctly:

.. GENERATED FROM PYTHON SOURCE LINES 107-112

.. code-block:: Python

    mesh["depth"] = mesh.depth(use_coords=True)
    fig = mesh.plot_contourf("depth")
    mesh["pressure"] = mesh.p_fluid()
    fig = mesh.plot_contourf(ot.variables.pressure)




.. rst-class:: sphx-glr-horizontal


    *

      .. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_011.png
         :alt: plot solid mechanics
         :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_011.png
         :class: sphx-glr-multi-img

    *

      .. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_012.png
         :alt: plot solid mechanics
         :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_012.png
         :class: sphx-glr-multi-img


.. rst-class:: sphx-glr-script-out

 .. code-block:: none

    /builds/ogs/tools/ogstools/.venv-devcontainer/lib/python3.10/site-packages/pyvista/core/dataset.py:2020: UnitStrippedWarning: The unit of the quantity is stripped when downcasting to ndarray.
      scalars = np.asanyarray(scalars)




.. GENERATED FROM PYTHON SOURCE LINES 113-116

Dilantancy criterion
--------------------
see: :py:func:`ogstools.variables.mesh_dependent.dilatancy_critescu`

.. GENERATED FROM PYTHON SOURCE LINES 118-121

.. code-block:: Python

    fig = mesh.plot_contourf(ot.variables.dilatancy_critescu_tot)
    fig = mesh.plot_contourf(ot.variables.dilatancy_critescu_eff)




.. rst-class:: sphx-glr-horizontal


    *

      .. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_013.png
         :alt: plot solid mechanics
         :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_013.png
         :class: sphx-glr-multi-img

    *

      .. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_014.png
         :alt: plot solid mechanics
         :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_014.png
         :class: sphx-glr-multi-img





.. GENERATED FROM PYTHON SOURCE LINES 122-125

Fluid pressure criterion
------------------------
see: :py:func:`ogstools.variables.mesh_dependent.fluid_pressure_criterion`

.. GENERATED FROM PYTHON SOURCE LINES 127-128

.. code-block:: Python

    fig = mesh.plot_contourf(ot.variables.fluid_pressure_crit)



.. image-sg:: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_015.png
   :alt: plot solid mechanics
   :srcset: /auto_examples/howto_quickstart/images/sphx_glr_plot_solid_mechanics_015.png
   :class: sphx-glr-single-img






.. rst-class:: sphx-glr-timing

   **Total running time of the script:** (0 minutes 3.971 seconds)


.. _sphx_glr_download_auto_examples_howto_quickstart_plot_solid_mechanics.py:

.. only:: html

  .. container:: sphx-glr-footer sphx-glr-footer-example

    .. container:: sphx-glr-download sphx-glr-download-jupyter

      :download:`Download Jupyter notebook: plot_solid_mechanics.ipynb <plot_solid_mechanics.ipynb>`

    .. container:: sphx-glr-download sphx-glr-download-python

      :download:`Download Python source code: plot_solid_mechanics.py <plot_solid_mechanics.py>`

    .. container:: sphx-glr-download sphx-glr-download-zip

      :download:`Download zipped: plot_solid_mechanics.zip <plot_solid_mechanics.zip>`
