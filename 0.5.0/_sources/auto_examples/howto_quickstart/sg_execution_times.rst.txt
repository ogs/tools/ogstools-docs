
:orphan:

.. _sphx_glr_auto_examples_howto_quickstart_sg_execution_times:


Computation times
=================
**00:05.036** total execution time for 2 files **from auto_examples/howto_quickstart**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_auto_examples_howto_quickstart_plot_solid_mechanics.py` (``plot_solid_mechanics.py``)
     - 00:03.971
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_quickstart_plot_meshseries.py` (``plot_meshseries.py``)
     - 00:01.065
     - 0.0
