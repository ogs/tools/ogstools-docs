
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "auto_examples/howto_plot/plot_observation_points.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        :ref:`Go to the end <sphx_glr_download_auto_examples_howto_plot_plot_observation_points.py>`
        to download the full example code.

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_auto_examples_howto_plot_plot_observation_points.py:


How to plot data at observation points
======================================

.. sectionauthor:: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)

In this example we plot the data values on observation points over all
timesteps. Since requested observation points don't necessarily coincide with
actual nodes of the mesh different interpolation options are available. See
:py:mod:`ogstools.meshlib.mesh_series.MeshSeries.probe` for more details.
Here we use a component transport example from the ogs benchmark gallery
(https://www.opengeosys.org/docs/benchmarks/hydro-component/elder/).

.. GENERATED FROM PYTHON SOURCE LINES 16-27

.. code-block:: Python



    import matplotlib.pyplot as plt
    import numpy as np

    import ogstools as ogs
    from ogstools import examples

    mesh_series = examples.load_meshseries_CT_2D_XDMF()
    si = ogs.variables.saturation








.. GENERATED FROM PYTHON SOURCE LINES 34-44

To read your own data as a mesh series you can do:

..  code-block:: python

  from ogstools.meshlib import MeshSeries
  mesh_series = MeshSeries("filepath/filename_pvd_or_xdmf")

You can also use a variable from the available presets instead of needing to
create your own:
:ref:`sphx_glr_auto_examples_howto_postprocessing_plot_variables.py`

.. GENERATED FROM PYTHON SOURCE LINES 46-47

Let's define 4 observation points and plot them on the mesh.

.. GENERATED FROM PYTHON SOURCE LINES 49-59

.. code-block:: Python

    points = np.asarray(
        [[x, 0, 60] for x in [0, 40, 80, 120]]
        + [[x, 0, 40] for x in [0, 40, 80, 120]]
    )
    fig = mesh_series.mesh(0).plot_contourf(si)
    fig.axes[0].scatter(points[:, 0], points[:, 2], s=50, fc="none", ec="r", lw=3)
    for i, point in enumerate(points):
        fig.axes[0].annotate(str(i), (point[0], point[2] - 5), va="top")
    plt.rcdefaults()  # TODO




.. image-sg:: /auto_examples/howto_plot/images/sphx_glr_plot_observation_points_001.png
   :alt: plot observation points
   :srcset: /auto_examples/howto_plot/images/sphx_glr_plot_observation_points_001.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 60-61

And now probe the points and the values over time:

.. GENERATED FROM PYTHON SOURCE LINES 63-67

.. code-block:: Python

    labels = [f"{i}: {label}" for i, label in enumerate(ogs.plot.utils.justified_labels(points))]
    fig = mesh_series.plot_probe(
        points=points[:4], variable=si, time_unit="a", labels=labels[:4]
    )



.. image-sg:: /auto_examples/howto_plot/images/sphx_glr_plot_observation_points_002.png
   :alt: plot observation points
   :srcset: /auto_examples/howto_plot/images/sphx_glr_plot_observation_points_002.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 68-75

You can also pass create your own matplotlib figure and pass the axes object.
Additionally, you can pass any keyword arguments which are known by
matplotlibs plot function to further customize the curves.
In this case `marker` and `linewidth` are not part of the API of `plot_probe`
but get processed correctly anyway.
If you want to have more freedom with the data you can just do the probing,
adapt to your needs and then do the plotting yourself:

.. GENERATED FROM PYTHON SOURCE LINES 77-92

.. code-block:: Python

    fig, axs = plt.subplots(nrows=2, figsize=[10, 5])
    mesh_series.plot_probe(
        points[:4], si, time_unit="a", ax=axs[0], colors=["k"],
        labels=labels[:4], marker=".")
    mesh_series.plot_probe(
        points[4:], si, time_unit="a", ax=axs[1], linestyles=["-"],
        labels=labels[4:], linewidth=1,
    )
    # add the mean of the observation point timeseries
    values = si.transform(mesh_series.probe(points, data_name=si.data_name))
    mean_values = np.mean(values.reshape((-1, 2, 4)), axis=-1)
    ts = mesh_series.timevalues("a")
    for index in range(2):
        fig.axes[index].plot(ts, mean_values[:, index], "rk"[index], label="mean")
        fig.axes[index].legend()



.. image-sg:: /auto_examples/howto_plot/images/sphx_glr_plot_observation_points_003.png
   :alt: plot observation points
   :srcset: /auto_examples/howto_plot/images/sphx_glr_plot_observation_points_003.png
   :class: sphx-glr-single-img






.. rst-class:: sphx-glr-timing

   **Total running time of the script:** (0 minutes 0.873 seconds)


.. _sphx_glr_download_auto_examples_howto_plot_plot_observation_points.py:

.. only:: html

  .. container:: sphx-glr-footer sphx-glr-footer-example

    .. container:: sphx-glr-download sphx-glr-download-jupyter

      :download:`Download Jupyter notebook: plot_observation_points.ipynb <plot_observation_points.ipynb>`

    .. container:: sphx-glr-download sphx-glr-download-python

      :download:`Download Python source code: plot_observation_points.py <plot_observation_points.py>`

    .. container:: sphx-glr-download sphx-glr-download-zip

      :download:`Download zipped: plot_observation_points.zip <plot_observation_points.zip>`
