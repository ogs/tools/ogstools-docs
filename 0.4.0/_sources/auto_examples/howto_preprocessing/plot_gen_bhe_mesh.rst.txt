
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "auto_examples/howto_preprocessing/plot_gen_bhe_mesh.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        :ref:`Go to the end <sphx_glr_download_auto_examples_howto_preprocessing_plot_gen_bhe_mesh.py>`
        to download the full example code.

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_auto_examples_howto_preprocessing_plot_gen_bhe_mesh.py:


Creating a BHE mesh (Borehole Heat Exchanger)
===================================================

.. GENERATED FROM PYTHON SOURCE LINES 8-10

This example demonstrates how to use :py:mod:`ogstools.meshlib.gmsh_meshing.gen_bhe_mesh` to create
a Borehole Heat Exchanger (BHE) mesh.

.. GENERATED FROM PYTHON SOURCE LINES 12-20

.. code-block:: Python

    from pathlib import Path
    from tempfile import mkdtemp

    import pyvista as pv
    from pyvista.plotting import Plotter

    from ogstools.meshlib.gmsh_meshing import BHE, Groundwater, gen_bhe_mesh








.. GENERATED FROM PYTHON SOURCE LINES 21-31

Introduction
----------------
This example shows the general usage of :py:mod:`ogstools.meshlib.gmsh_meshing.gen_bhe_mesh` and how some of
the parameters will effect the mesh. This section demonstrates the mesh
generation with only three soil layers, groundwater flow in one layer
and three BHE's. However, this tool provides multiple soil layers,
groundwater flow in multiple layers and multiple BHE's. The mesh sizes
provides good initial values for the most Heat-Transport-BHE simulations
in OGS. They can also be set by the user, to customize the mesh.
Feel free to try it out!

.. GENERATED FROM PYTHON SOURCE LINES 33-36

Create a simple prism mesh
--------------------------------
Generate a customizable prism BHE mesh:

.. GENERATED FROM PYTHON SOURCE LINES 38-56

.. code-block:: Python

    tmp_dir = Path(mkdtemp())
    vtu_file = tmp_dir / "bhe_prism.vtu"
    bhe_meshes = gen_bhe_mesh(
        length=150,
        width=100,
        layer=[50, 50, 50],
        groundwater=Groundwater(
            begin=-30, isolation_layer_id=1, flow_direction="+x"
        ),
        BHE_Array=[
            BHE(x=50, y=40, z_begin=-1, z_end=-60, borehole_radius=0.076),
            BHE(x=50, y=50, z_begin=-1, z_end=-60, borehole_radius=0.076),
            BHE(x=50, y=60, z_begin=-1, z_end=-60, borehole_radius=0.076),
        ],
        meshing_type="prism",
        out_name=vtu_file,
    )








.. GENERATED FROM PYTHON SOURCE LINES 57-58

Load the domain mesh and all submeshes as well as extract the BHE lines:

.. GENERATED FROM PYTHON SOURCE LINES 60-66

.. code-block:: Python

    mesh = pv.read(tmp_dir / bhe_meshes[0])
    top_mesh = pv.read(tmp_dir / bhe_meshes[1])
    bottom_mesh = pv.read(tmp_dir / bhe_meshes[2])
    gw_mesh = pv.read(tmp_dir / bhe_meshes[3])
    bhe_line = mesh.extract_cells_by_type(pv.CellType.LINE)








.. GENERATED FROM PYTHON SOURCE LINES 67-68

Visualize the mesh:

.. GENERATED FROM PYTHON SOURCE LINES 70-91

.. code-block:: Python

    top_mesh = top_mesh.translate((0, 0, 10), inplace=False)
    bottom_mesh = bottom_mesh.translate((0, 0, -10), inplace=False)
    gw_mesh = gw_mesh.translate((-10, 0, 0), inplace=False)

    p = Plotter()
    p.add_mesh(mesh, style="wireframe", color="grey")
    p.add_mesh(
        mesh.clip("x", bhe_line.center, crinkle=True),
        show_edges=True,
        scalars="MaterialIDs",
        cmap="Accent",
        categories=True,
        scalar_bar_args={"vertical": True, "n_labels": 2, "fmt": "%.0f"},
    )
    p.add_mesh(bhe_line, color="r", line_width=3)
    p.add_mesh(top_mesh, show_edges=True)
    p.add_mesh(bottom_mesh, show_edges=True)
    p.add_mesh(gw_mesh, show_edges=True)
    p.add_axes()
    p.show()








.. tab-set::



   .. tab-item:: Static Scene



            
     .. image-sg:: /auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_001.png
        :alt: plot gen bhe mesh
        :srcset: /auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_001.png
        :class: sphx-glr-single-img
     


   .. tab-item:: Interactive Scene



       .. offlineviewer:: /builds/ogs/tools/ogstools/docs/auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_001.vtksz






.. GENERATED FROM PYTHON SOURCE LINES 92-95

Create a simple structured mesh
-----------------------------------
Generate a customizable structured BHE mesh:

.. GENERATED FROM PYTHON SOURCE LINES 97-115

.. code-block:: Python

    tmp_dir = Path(mkdtemp())
    vtu_file = tmp_dir / "bhe_structured.vtu"
    bhe_meshes = gen_bhe_mesh(
        length=150,
        width=100,
        layer=[50, 50, 50],
        groundwater=Groundwater(
            begin=-30, isolation_layer_id=1, flow_direction="+x"
        ),
        BHE_Array=[
            BHE(x=50, y=40, z_begin=-1, z_end=-60, borehole_radius=0.076),
            BHE(x=50, y=50, z_begin=-1, z_end=-60, borehole_radius=0.076),
            BHE(x=50, y=60, z_begin=-1, z_end=-60, borehole_radius=0.076),
        ],
        meshing_type="structured",
        out_name=vtu_file,
    )








.. GENERATED FROM PYTHON SOURCE LINES 116-117

Load the domain mesh and all submeshes as well as extract the BHE lines:

.. GENERATED FROM PYTHON SOURCE LINES 119-125

.. code-block:: Python

    mesh = pv.read(tmp_dir / bhe_meshes[0])
    top_mesh = pv.read(tmp_dir / bhe_meshes[1])
    bottom_mesh = pv.read(tmp_dir / bhe_meshes[2])
    gw_mesh = pv.read(tmp_dir / bhe_meshes[3])
    bhe_line = mesh.extract_cells_by_type(pv.CellType.LINE)








.. GENERATED FROM PYTHON SOURCE LINES 126-127

Visualize the mesh:

.. GENERATED FROM PYTHON SOURCE LINES 129-150

.. code-block:: Python

    top_mesh = top_mesh.translate((0, 0, 10), inplace=False)
    bottom_mesh = bottom_mesh.translate((0, 0, -10), inplace=False)
    gw_mesh = gw_mesh.translate((-10, 0, 0), inplace=False)

    p = Plotter()
    p.add_mesh(mesh, style="wireframe", color="grey")
    p.add_mesh(
        mesh.clip("x", bhe_line.center, crinkle=True),
        show_edges=True,
        scalars="MaterialIDs",
        cmap="Accent",
        categories=True,
        scalar_bar_args={"vertical": True, "n_labels": 2, "fmt": "%.0f"},
    )
    p.add_mesh(bhe_line, color="r", line_width=3)
    p.add_mesh(top_mesh, show_edges=True)
    p.add_mesh(bottom_mesh, show_edges=True)
    p.add_mesh(gw_mesh, show_edges=True)
    p.add_axes()
    p.show()








.. tab-set::



   .. tab-item:: Static Scene



            
     .. image-sg:: /auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_002.png
        :alt: plot gen bhe mesh
        :srcset: /auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_002.png
        :class: sphx-glr-single-img
     


   .. tab-item:: Interactive Scene



       .. offlineviewer:: /builds/ogs/tools/ogstools/docs/auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_002.vtksz






.. GENERATED FROM PYTHON SOURCE LINES 151-156

Create an advanced structured mesh
--------------------------------------
Generate a customizable structured BHE mesh with advanced mesh
sizing options (using gmsh). To understand the specific
behaviour of every mesh parameter, test each one after another.

.. GENERATED FROM PYTHON SOURCE LINES 158-180

.. code-block:: Python

    vtu_file = tmp_dir / "bhe_structured_advanced.vtu"
    bhe_meshes = gen_bhe_mesh(
        length=150,
        width=100,
        layer=[50, 50, 50],
        groundwater=Groundwater(-30, 1, "+x"),
        BHE_Array=[
            BHE(50, 40, -1, -60, 0.076),
            BHE(50, 50, -1, -60, 0.076),
            BHE(50, 60, -1, -60, 0.076),
        ],
        meshing_type="structured",
        target_z_size_coarse=10,  # default value 7.5
        target_z_size_fine=2,  # default value 1.5
        n_refinement_layers=1,  # default value 2
        dist_box_x=15,  # default value 10
        propagation=1.2,  # default value 1.1
        inner_mesh_size=8,  # default value 5
        outer_mesh_size=12,  # default value 10
        out_name=vtu_file,
    )








.. GENERATED FROM PYTHON SOURCE LINES 181-182

Load the domain mesh and all submeshes as well as extract the BHE lines:

.. GENERATED FROM PYTHON SOURCE LINES 184-190

.. code-block:: Python

    mesh = pv.read(tmp_dir / bhe_meshes[0])
    top_mesh = pv.read(tmp_dir / bhe_meshes[1])
    bottom_mesh = pv.read(tmp_dir / bhe_meshes[2])
    gw_mesh = pv.read(tmp_dir / bhe_meshes[3])
    bhe_line = mesh.extract_cells_by_type(pv.CellType.LINE)








.. GENERATED FROM PYTHON SOURCE LINES 191-192

Visualize the mesh:

.. GENERATED FROM PYTHON SOURCE LINES 194-214

.. code-block:: Python

    top_mesh = top_mesh.translate((0, 0, 10), inplace=False)
    bottom_mesh = bottom_mesh.translate((0, 0, -10), inplace=False)
    gw_mesh = gw_mesh.translate((-10, 0, 0), inplace=False)

    p = Plotter()
    p.add_mesh(mesh, style="wireframe", color="grey")
    p.add_mesh(
        mesh.clip("x", bhe_line.center, crinkle=True),
        show_edges=True,
        scalars="MaterialIDs",
        cmap="Accent",
        categories=True,
        scalar_bar_args={"vertical": True, "n_labels": 2, "fmt": "%.0f"},
    )
    p.add_mesh(bhe_line, color="r", line_width=3)
    p.add_mesh(top_mesh, show_edges=True)
    p.add_mesh(bottom_mesh, show_edges=True)
    p.add_mesh(gw_mesh, show_edges=True)
    p.add_axes()
    p.show()







.. tab-set::



   .. tab-item:: Static Scene



            
     .. image-sg:: /auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_003.png
        :alt: plot gen bhe mesh
        :srcset: /auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_003.png
        :class: sphx-glr-single-img
     


   .. tab-item:: Interactive Scene



       .. offlineviewer:: /builds/ogs/tools/ogstools/docs/auto_examples/howto_preprocessing/images/sphx_glr_plot_gen_bhe_mesh_003.vtksz







.. rst-class:: sphx-glr-timing

   **Total running time of the script:** (0 minutes 2.659 seconds)


.. _sphx_glr_download_auto_examples_howto_preprocessing_plot_gen_bhe_mesh.py:

.. only:: html

  .. container:: sphx-glr-footer sphx-glr-footer-example

    .. container:: sphx-glr-download sphx-glr-download-jupyter

      :download:`Download Jupyter notebook: plot_gen_bhe_mesh.ipynb <plot_gen_bhe_mesh.ipynb>`

    .. container:: sphx-glr-download sphx-glr-download-python

      :download:`Download Python source code: plot_gen_bhe_mesh.py <plot_gen_bhe_mesh.py>`

    .. container:: sphx-glr-download sphx-glr-download-zip

      :download:`Download zipped: plot_gen_bhe_mesh.zip <plot_gen_bhe_mesh.zip>`
