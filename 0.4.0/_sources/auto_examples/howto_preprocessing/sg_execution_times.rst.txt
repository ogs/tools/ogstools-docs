
:orphan:

.. _sphx_glr_auto_examples_howto_preprocessing_sg_execution_times:


Computation times
=================
**00:16.271** total execution time for 6 files **from auto_examples/howto_preprocessing**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_auto_examples_howto_preprocessing_plot_shapefile_meshing.py` (``plot_shapefile_meshing.py``)
     - 00:06.251
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_preprocessing_plot_remeshing.py` (``plot_remeshing.py``)
     - 00:03.636
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_preprocessing_plot_meshlib_vtu_input.py` (``plot_meshlib_vtu_input.py``)
     - 00:03.308
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_preprocessing_plot_gen_bhe_mesh.py` (``plot_gen_bhe_mesh.py``)
     - 00:02.659
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_preprocessing_plot_meshlib_pyvista_input.py` (``plot_meshlib_pyvista_input.py``)
     - 00:00.417
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_preprocessing_plot_terrain_meshing.py` (``plot_terrain_meshing.py``)
     - 00:00.000
     - 0.0
