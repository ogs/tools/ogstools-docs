

.. _sphx_glr_auto_examples_howto_simulation:

Running the simulation
======================

This section covers tools, that can be used to monitor OGS simulations at
runtime. Examples about running simulations from Python will be added in the
future.



.. raw:: html

    <div class="sphx-glr-thumbnails">

.. thumbnail-parent-div-open

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="This simple example demonstrates how to analyze the OGS (OpenGeoSys) log output to extract performance information regarding various computational parts of OGS. Here we utilize the project file from the benchmark titled: OGS: Constant viscosity (Hydro-Thermal)">

.. only:: html

  .. image:: /auto_examples/howto_simulation/images/thumb/sphx_glr_plot_100_logparser_intro_thumb.png
    :alt:

  :ref:`sphx_glr_auto_examples_howto_simulation_plot_100_logparser_intro.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Logparser: Introduction</div>
    </div>


.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="In this section, we showcase various predefined analyses available in the log parser. We utilize project files from the following benchmarks: ogs: Constant viscosity (Hydro-Thermal) and for the staggered scheme we use a prj from ogs tests: HT StaggeredCoupling HeatTransportInStationaryFlow">

.. only:: html

  .. image:: /auto_examples/howto_simulation/images/thumb/sphx_glr_plot_101_logparser_analyses_thumb.png
    :alt:

  :ref:`sphx_glr_auto_examples_howto_simulation_plot_101_logparser_analyses.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Logparser: Predefined Analyses</div>
    </div>


.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="We address to following:">

.. only:: html

  .. image:: /auto_examples/howto_simulation/images/thumb/sphx_glr_plot_102_logparser_advanced_thumb.png
    :alt:

  :ref:`sphx_glr_auto_examples_howto_simulation_plot_102_logparser_advanced.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Logparser: Advanced topics</div>
    </div>


.. thumbnail-parent-div-close

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /auto_examples/howto_simulation/plot_100_logparser_intro
   /auto_examples/howto_simulation/plot_101_logparser_analyses
   /auto_examples/howto_simulation/plot_102_logparser_advanced

