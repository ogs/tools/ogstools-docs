{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Feflowlib: Component-transport model - conversion and simulation\n.. sectionauthor:: Julian Heinze (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nIn this example we show how a simple mass transport FEFLOW model can be converted to a pyvista.UnstructuredGrid and then\nbe simulated in OGS with the component transport process.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "0. Necessary imports\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import tempfile\nimport xml.etree.ElementTree as ET\nfrom pathlib import Path\n\nimport ifm_contrib as ifm\nimport matplotlib.pyplot as plt\nimport numpy as np\n\nimport ogstools as ogs\nfrom ogstools.examples import feflow_model_2D_CT_t_560\nfrom ogstools.feflowlib import (\n    component_transport,\n    convert_properties_mesh,\n    get_material_properties_of_CT_model,\n    get_species,\n    setup_prj_file,\n    write_point_boundary_conditions,\n)\nfrom ogstools.meshlib import Mesh\n\nogs.plot.setup.show_element_edges = True"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1. Load a FEFLOW model (.fem) as a FEFLOW document, convert and save it. More details on\nhow the conversion function works can be found here: :py:mod:`ogstools.feflowlib.convert_properties_mesh`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "feflow_model = ifm.loadDocument(str(feflow_model_2D_CT_t_560))\nfeflow_pv_mesh = convert_properties_mesh(feflow_model)\n\ntemp_dir = Path(tempfile.mkdtemp(\"feflow_test_simulation\"))\nfeflow_mesh_file = temp_dir / \"2D_CT_model.vtu\"\nfeflow_pv_mesh.save(feflow_mesh_file)\n\nfeflow_concentration = ogs.variables.Scalar(\n    data_name=\"single_species_P_CONC\",\n    output_name=\"concentration\",\n    data_unit=\"mg/l\",\n    output_unit=\"mg/l\",\n)\n# The original mesh is clipped to focus on the relevant part of it, where concentration is larger\n# than 1e-9 mg/l. The rest of the mesh has concentration values of 0.\nogs.plot.contourf(\n    feflow_pv_mesh.clip_scalar(\n        scalars=\"single_species_P_CONC\", invert=False, value=1.0e-9\n    ),\n    feflow_concentration,\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "2. Save the point boundary conditions (see: :py:mod:`ogstools.feflowlib.write_point_boundary_conditions`).\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "write_point_boundary_conditions(temp_dir, feflow_pv_mesh)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "3. Setup a prj-file (see: :py:mod:`ogstools.feflowlib.setup_prj_file`) to run a OGS-simulation.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "path_prjfile = feflow_mesh_file.with_suffix(\".prj\")\nprj = ogs.Project(output_file=path_prjfile)\nspecies = get_species(feflow_pv_mesh)\nCT_model = component_transport(\n    saving_path=temp_dir / \"sim_2D_CT_model\",\n    species=species,\n    prj=prj,\n    dimension=2,\n    fixed_out_times=[48384000],\n    initial_time=0,\n    end_time=int(4.8384e07),\n    time_stepping=list(\n        zip([10] * 8, [8.64 * 10**i for i in range(8)], strict=False)\n    ),\n)\n# Include the mesh specific configurations to the template.\nmodel = setup_prj_file(\n    bulk_mesh_path=feflow_mesh_file,\n    mesh=feflow_pv_mesh,\n    material_properties=get_material_properties_of_CT_model(feflow_pv_mesh),\n    process=\"component transport\",\n    species_list=species,\n    model=CT_model,\n    max_iter=6,\n    rel_tol=1e-14,\n)\n# The model must be written before it can be run.\nmodel.write_input(path_prjfile)\n# Print the prj-file as an example.\nET.dump(ET.parse(path_prjfile))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "4. Run the model.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "model.run_model(logfile=temp_dir / \"out.log\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "5. Read the results along a line on the upper edge of the mesh parallel to the x-axis and plot them.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "ms = ogs.MeshSeries(temp_dir / \"sim_2D_CT_model.pvd\")\n# Read the last timestep:\nogs_sim_res = ms.mesh(ms.timesteps[-1])\n\"\"\"\nIt is also possible to read the file directly with pyvista:\nogs_sim_res = pv.read(\n   temp_dir / \"sim_2D_CT_model_ts_65_t_48384000.000000.vtu\"\n)\n\"\"\"\nprofile = np.array([[0.038 + 1.0e-8, 0.005, 0], [0.045, 0.005, 0]])\nfig, ax = plt.subplots(1, 1, figsize=(7, 5))\nogs_sim_res.plot_linesample(\n    \"dist\",\n    ogs.variables.Scalar(\n        data_name=\"single_species\",\n        output_name=\"concentration\",\n        data_unit=\"mg/l\",\n        output_unit=\"mg/l\",\n    ),\n    profile_points=profile,\n    ax=ax,\n    resolution=1000,\n    grid=\"major\",\n    fontsize=18,\n    label=\"OGS\",\n    color=\"black\",\n    linewidth=2,\n)\nMesh(feflow_pv_mesh).plot_linesample(\n    \"dist\",\n    feflow_concentration,\n    profile_points=profile,\n    ax=ax,\n    resolution=1000,\n    fontsize=16,\n    label=\"FEFLOW\",\n    ls=\":\",\n    linewidth=2,\n    color=\"red\",\n)\nax.legend(loc=\"best\", fontsize=16)\nfig.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6. Concentration difference plotted on the mesh.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "ogs_sim_res[\"concentration_difference\"] = (\n    feflow_pv_mesh[\"single_species_P_CONC\"] - ogs_sim_res[\"single_species\"]\n)\nconcentration_difference = ogs.variables.Scalar(\n    data_name=\"concentration_difference\",\n    output_name=\"concentration\",\n    data_unit=\"mg/l\",\n    output_unit=\"mg/l\",\n)\n\nbounds = [0.038, 0.045, 0, 0.01, 0, 0]\n\nogs.plot.contourf(\n    ogs_sim_res.clip_box(bounds, invert=False),\n    concentration_difference,\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6.1 Concentration difference plotted along a line.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig, ax = plt.subplots(1, 1, figsize=(7, 5))\nogs_sim_res.plot_linesample(\n    \"dist\",\n    concentration_difference,\n    profile_points=profile,\n    ax=ax,\n    resolution=1000,\n    grid=\"both\",\n    fontsize=18,\n    linewidth=2,\n    color=\"green\",\n    label=\"Difference FEFLOW-OGS\",\n)\nax.legend(loc=\"best\", fontsize=16)\nfig.tight_layout()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}