{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Feflowlib: Hydraulic model - conversion and simulation\n\n.. sectionauthor:: Julian Heinze (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nIn this example we show how a simple flow/hydraulic FEFLOW model can be converted to a pyvista.UnstructuredGrid and then\nbe simulated in OGS.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "0. Necessary imports\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import tempfile\nimport xml.etree.ElementTree as ET\nfrom pathlib import Path\n\nimport ifm_contrib as ifm\nimport numpy as np\nimport pyvista as pv\n\nimport ogstools as ogs\nfrom ogstools.examples import feflow_model_box_Neumann\nfrom ogstools.feflowlib import (\n    convert_properties_mesh,\n    extract_cell_boundary_conditions,\n    setup_prj_file,\n    steady_state_diffusion,\n)\nfrom ogstools.feflowlib.tools import (\n    extract_point_boundary_conditions,\n    get_material_properties,\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1. Load a FEFLOW model (.fem) as a FEFLOW document, convert and save it. More details on\nhow the conversion function works can be found here: :py:mod:`ogstools.feflowlib.convert_properties_mesh`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "feflow_model = ifm.loadDocument(str(feflow_model_box_Neumann))\npyvista_mesh = convert_properties_mesh(feflow_model)\n\npv.global_theme.colorbar_orientation = \"vertical\"\npyvista_mesh.plot(\n    show_edges=True,\n    off_screen=True,\n    scalars=\"P_HEAD\",\n    cpos=[0, 1, 0.5],\n    scalar_bar_args={\"position_x\": 0.1, \"position_y\": 0.25},\n)\nprint(pyvista_mesh)\ntemp_dir = Path(tempfile.mkdtemp(\"feflow_test_simulation\"))\nfeflow_mesh_file = temp_dir / \"boxNeumann.vtu\"\npyvista_mesh.save(feflow_mesh_file)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "2. Extract the point conditions (see: :py:mod:`ogstools.feflowlib.extract_point_boundary_conditions`).\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "point_BC_dict = extract_point_boundary_conditions(temp_dir, pyvista_mesh)\n# Since there can be multiple point boundary conditions on the bulk mesh,\n# they are saved and plotted iteratively.\nplotter = pv.Plotter(shape=(len(point_BC_dict), 1))\nfor i, (path, boundary_condition) in enumerate(point_BC_dict.items()):\n    boundary_condition.save(path)\n    plotter.subplot(i, 0)\n    plotter.add_mesh(boundary_condition, scalars=Path(path).stem)\nplotter.show()\npath_topsurface, topsurface = extract_cell_boundary_conditions(\n    feflow_mesh_file, pyvista_mesh\n)\n# On the topsurface can be cell based boundary condition.\n# The boundary conditions on the topsurface of the model are required for generalization.\ntopsurface.save(path_topsurface)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "3. Setup a prj-file (see: :py:mod:`ogstools.feflowlib.setup_prj_file`) to run a OGS-simulation.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "path_prjfile = feflow_mesh_file.with_suffix(\".prj\")\nprj = ogs.Project(output_file=path_prjfile)\n# Get the template prj-file configurations for a steady state diffusion process\nssd_model = steady_state_diffusion(temp_dir / \"sim_boxNeumann\", prj)\n# Include the mesh specific configurations to the template.\nmodel = setup_prj_file(\n    bulk_mesh_path=feflow_mesh_file,\n    mesh=pyvista_mesh,\n    material_properties=get_material_properties(pyvista_mesh, \"P_CONDX\"),\n    process=\"steady state diffusion\",\n    model=ssd_model,\n)\n# The model must be written before it can be run.\nmodel.write_input(path_prjfile)\n# Simply print the prj-file as an example.\nmodel_prjfile = ET.parse(path_prjfile)\nET.dump(model_prjfile)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "4. Run the model\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "model.run_model(logfile=temp_dir / \"out.log\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "5. Read the results and plot them.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "ms = ogs.MeshSeries(temp_dir / \"sim_boxNeumann.pvd\")\n# Read the last timestep:\nogs_sim_res = ms.mesh(ms.timesteps[-1])\n\"\"\"\nIt is also possible to read the file directly with pyvista:\nogs_sim_res = pv.read(temp_dir / \"sim_boxNeumann_ts_1_t_1.000000.vtu\")\n\"\"\"\nogs_sim_res.plot(\n    show_edges=True,\n    off_screen=True,\n    scalars=\"HEAD_OGS\",\n    cpos=[0, 1, 0.5],\n    scalar_bar_args={\"position_x\": 0.1, \"position_y\": 0.25},\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "5.1 Plot the hydraulic head simulated in OGS with :py:mod:`ogstools.plot.contourf`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "head = ogs.variables.Scalar(\n    data_name=\"HEAD_OGS\", data_unit=\"m\", output_unit=\"m\"\n)\nfig = ogs.plot.contourf(ogs_sim_res.slice(normal=\"z\", origin=[50, 50, 0]), head)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6. Calculate the difference to the FEFLOW simulation and plot it.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "diff = pyvista_mesh[\"P_HEAD\"] - ogs_sim_res[\"HEAD_OGS\"]\npyvista_mesh[\"diff_HEAD\"] = diff\npyvista_mesh.plot(\n    show_edges=True,\n    off_screen=True,\n    scalars=\"diff_HEAD\",\n    cpos=[0, 1, 0.5],\n    scalar_bar_args={\"position_x\": 0.1, \"position_y\": 0.25},\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6.1 Plot the differences in the hydraulic head with :py:mod:`ogstools.plot.contourf`.\nSlices are taken along the z-axis.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "diff_head = ogs.variables.Scalar(\n    data_name=\"diff_HEAD\", data_unit=\"m\", output_unit=\"m\"\n)\nslices = np.reshape(list(pyvista_mesh.slice_along_axis(n=4, axis=\"z\")), (2, 2))\nfig = ogs.plot.contourf(slices, diff_head)\nfor ax, slice in zip(fig.axes, np.ravel(slices), strict=False):\n    ax.set_title(f\"z = {slice.center[2]:.1f} {ms.spatial_output_unit}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Slices are taken along the y-axis.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "slices = np.reshape(list(pyvista_mesh.slice_along_axis(n=4, axis=\"y\")), (2, 2))\nfig = ogs.plot.contourf(slices, diff_head)\nfor ax, slice in zip(fig.axes, np.ravel(slices), strict=False):\n    ax.set_title(f\"y = {slice.center[1]:.1f} {ms.spatial_output_unit}\")"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}