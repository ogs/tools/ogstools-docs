
:orphan:

.. _sphx_glr_auto_examples_howto_meshplotlib_sg_execution_times:


Computation times
=================
**00:23.981** total execution time for **auto_examples_howto_meshplotlib** files:

+-----------------------------------------------------------------------------------------------------+-----------+--------+
| :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_animation.py` (``plot_animation.py``)           | 00:12.252 | 0.0 MB |
+-----------------------------------------------------------------------------------------------------+-----------+--------+
| :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_meshplotlib_3d.py` (``plot_meshplotlib_3d.py``) | 00:07.530 | 0.0 MB |
+-----------------------------------------------------------------------------------------------------+-----------+--------+
| :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_meshplotlib_2d.py` (``plot_meshplotlib_2d.py``) | 00:04.199 | 0.0 MB |
+-----------------------------------------------------------------------------------------------------+-----------+--------+
