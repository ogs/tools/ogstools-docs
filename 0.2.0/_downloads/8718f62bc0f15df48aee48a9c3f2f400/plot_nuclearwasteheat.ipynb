{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Plotting nuclear waste heat over time\n\n.. sectionauthor:: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nFirst, some minimal example usage:\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import matplotlib.pyplot as plt\nimport numpy as np\n\nimport ogstools.physics.nuclearwasteheat as nuclear\n\nrepo = nuclear.repo_2020_conservative\nunits = {\"time_unit\": \"yrs\", \"power_unit\": \"kW\"}  # default is s and W\nprint(\"Heat at start of deposition (1 nuclear waste bundle): \")\nprint(f\"{0:6n} yrs: {repo.heat(t=0, **units):10.1f} kW\")\nprint(\"Heat after deposition complete (all nuclear waste bundles): \")\nprint(\n    f\"{repo.time_deposit('yrs'):6n} yrs: \"\n    f\"{repo.heat(t=repo.time_deposit('yrs'), **units):10.1f} kW\"\n)\nprint(\"Heat for a timeseries: \")\ntime = np.geomspace(1, 1e5, num=6)\nheat = repo.heat(t=time, **units)\nprint(*[f\"{t:6n} yrs: {q:10.1f} kW\" for t, q in zip(time, heat)], sep=\"\\n\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Now for the plotting define the timeframe and heat models of interest.\nAlso let's make a convenience function to format our plots.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "time = np.geomspace(1, 1e6, num=100)\nmodels = [model for model in nuclear.waste_types if \"2016\" not in model.name]\nls = [\"-\", \"--\", \"-.\", \":\", (0, (1, 10))]\n\n\ndef format_ax(ax: plt.Axes):\n    ax.set_xlabel(\"time / yrs\")\n    ax.set_ylabel(\"heat / kW\")\n    ax.grid(True, which=\"major\", linestyle=\"-\")\n    ax.grid(True, which=\"minor\", linestyle=\"--\", alpha=0.2)\n    ax.legend()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's compare the heat timeseries of single containers of different nuclear\nwaste types without interim storage or deposition taken into account\n(baseline=True).\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig, ax = plt.subplots(figsize=(8, 4))\nfor model in models:\n    q = model.heat(time, baseline=True, **units)\n    ax.loglog(time, q, label=model.name, lw=2.5)\nformat_ax(ax)\nax.set_ylim([1e-4, 5])\nplt.show()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The bumps in the curves stem from the different leading nuclides in the\nproxy model sequentially decaying to nothing. The leading nuclides don't\nnecessarily represent actual physical nuclides, but they give a close match\nto the result of burn-off simulations. We can visualize the decay of the\nnuclides themselves as well:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig, axs = plt.subplots(\n    nrows=int(0.5 + len(models) / 2), ncols=2, figsize=(16, 8), sharex=True\n)\naxs: list[plt.Axes] = np.reshape(axs, (-1))\n\ncolors = plt.rcParams[\"axes.prop_cycle\"].by_key()[\"color\"]\nfor ax, model, color in zip(axs, models, colors):\n    q = model.heat(time, baseline=True, **units)\n    ax.loglog(time, q, label=model.name, lw=2.5, c=color)\n    for i in range(len(model.nuclide_powers)):\n        q = model.heat(time, baseline=True, ncl_id=i, **units)\n        ax.loglog(time, q, label=f\"Nuclide {i}\", lw=1.5, c=color, ls=ls[i])\n    format_ax(ax)\n    ax.set_ylim([1e-4, 20])\nplt.show()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "When taking the interim storage time and the time to fill the repository\ninto account we get a linear increase of bundles adding to the total heat.\nIs is assumed, that each bundle has reached exactly the interim storage\ntime at the moment it is deposited. Let's compare the different available\nrepository models.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig, ax = plt.subplots(figsize=(8, 4))\n\nrepos = [\n    nuclear.repo_2020_conservative,\n    nuclear.repo_2020,\n    nuclear.repo_be_ha_2016,\n]\nrepo_heat = [repo.heat(time, **units) for repo in repos]\nax.loglog(time, repo_heat[0], \"k\", label=\"DWR-Mix conservative\", lw=2, ls=ls[0])\nax.loglog(time, repo_heat[1], \"k\", label=\"DWR-Mix + WWER + CSD\", lw=2, ls=ls[1])\nax.loglog(time, repo_heat[2], \"k\", label=\"RK-BE + RK-HA\", lw=2, ls=ls[2])\nformat_ax(ax)\nax.set_ylim([9, 25000])\nplt.show()"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig, ax = plt.subplots(figsize=(8, 2))\n\nax.loglog(time, repo_heat[0], label=\"DWR-Mix\", lw=2, c=\"k\")\nfor i in range(len(nuclear.repo_2020_conservative.waste[0].nuclide_powers)):\n    q = nuclear.repo_2020_conservative.heat(time, ncl_id=i, **units)\n    ax.loglog(time, q, label=f\"Nuclide {i}\", lw=1.5, c=\"k\", ls=ls[i])\nformat_ax(ax)\nax.set_ylim([9, 25000])\nplt.show()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.16"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}