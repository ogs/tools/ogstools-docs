{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Convergence study (spatial refinement)\n\nThis example shows one possible implementation of how to do a convergence study.\nIt uses the project file from the following benchmark with multiple\ndiscretizations to evaluate the accuracy of the numerical solutions.\n[ogs: elliptic neumann benchmark](https://www.opengeosys.org/docs/benchmarks/elliptic/elliptic-neumann/)\n\nHere is some theoretical background for the topic of grid convergence:\n\n[Nasa convergence reference](https://www.grc.nasa.gov/www/wind/valid/tutorial/spatconv.html)\n\n[More comprehensive reference](https://curiosityfluids.com/2016/09/09/establishing-grid-convergence/)\n\nAt least three meshes of increasing refinement are required for the convergence\nstudy. The three finest meshes are used to calculated the Richardson\nextrapolation. The third coarsest mesh will be used for the topology\nto evaluate the results. Its nodes should be shared by the finer meshes,\notherwise interpolation will influence the results. With\nunstructured grids this can be achieved as well with refinement by splitting.\n\nThe results to analyze are generated on the fly with the following code. If you\nare only interested in the convergence study, please skip to\n`Hydraulic pressure convergence`_.\n\nFirst, the required packages are imported and an output directory is created:\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from pathlib import Path\nfrom shutil import rmtree\nfrom tempfile import mkdtemp\n\nfrom IPython.display import HTML\nfrom ogs6py import ogs\n\nfrom ogstools import meshlib, meshplotlib, msh2vtu, propertylib, workflow\nfrom ogstools.studies import convergence\nfrom ogstools.studies.convergence.examples import (\n    steady_state_diffusion_analytical_solution,\n)\n\nmeshplotlib.setup.reset()\ntemp_dir = Path(mkdtemp(suffix=\"steady_state_diffusion\"))\nreport_name = str(temp_dir / \"report.ipynb\")\nresult_paths = []"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The meshes and their boundaries are generated easily via gmsh and\n:py:mod:`ogstools.msh2vtu`.\nThen we run the different simulations with increasingly fine spatial\ndiscretization via ogs6py and store the results for the convergence study.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "refinements = 6\nedge_cells = [2**i for i in range(refinements)]\nfor n_edge_cells in edge_cells:\n    msh_path = temp_dir / \"square.msh\"\n    meshlib.gmsh_meshing.rect(\n        n_edge_cells=n_edge_cells, structured_grid=True, out_name=msh_path\n    )\n    msh2vtu.msh2vtu(\n        input_filename=msh_path, output_path=temp_dir, log_level=\"ERROR\"\n    )\n\n    model = ogs.OGS(\n        PROJECT_FILE=temp_dir / \"default.prj\",\n        INPUT_FILE=convergence.examples.steady_state_diffusion_prj,\n    )\n    prefix = \"steady_state_diffusion_\" + str(n_edge_cells)\n    model.replace_text(prefix, \".//prefix\")\n    model.write_input()\n    ogs_args = f\"-m {temp_dir} -o {temp_dir}\"\n    model.run_model(write_logs=False, args=ogs_args)\n    result_paths += [str(temp_dir / (prefix + \".pvd\"))]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Here we calculate the analytical solution on one of the meshes:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "analytical_solution_path = temp_dir / \"analytical_solution.vtu\"\nsolution = steady_state_diffusion_analytical_solution(\n    meshlib.MeshSeries(result_paths[-1]).read(0)\n)\nmeshplotlib.setup.show_element_edges = True\nfig = meshplotlib.plot(solution, propertylib.presets.hydraulic_height)\nsolution.save(analytical_solution_path)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Hydraulic pressure convergence\n\nThe pressure field of this model is converging well. The convergence ratio\nis approximately 1 on the whole mesh and looking at the relative errors we\nsee a quadratic convergence behavior.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "convergence.run_convergence_study(\n    output_name=report_name,\n    mesh_paths=result_paths,\n    property_name=\"hydraulic_height\",\n    timevalue=1,\n    refinement_ratio=2.0,\n    reference_solution_path=str(analytical_solution_path),\n)\nHTML(workflow.jupyter_to_html(report_name, show_input=False))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Darcy velocity convergence\n\nFor the velocity we some discrepancy of the convergence ratio in the bottom\nright corner. Thus we know, at these points the mesh isn't properly\nconverging (at least for the velocity field).\nWe see, that in the bottom right corner, the velocity magnitude seems to be\nsteadily increasing, which is also reflected in the Richardson extrapolation,\nwhich shows an anomalous high value in this spot, hinting at a singularity\nthere. This is explained by the notion in the benchmark's documentation of\n\"incompatible boundary conditions imposed on the bottom right corner of the\ndomain.\" Regardless of this, the benchmark gives a convergent solution for\nthe pressure field.\nThe code cells from the templated notebook are show here for transparency.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "convergence.run_convergence_study(\n    output_name=report_name,\n    mesh_paths=result_paths,\n    property_name=\"velocity\",\n    timevalue=1,\n    refinement_ratio=2.0,\n)\nHTML(workflow.jupyter_to_html(report_name, show_input=True))"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.16"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}