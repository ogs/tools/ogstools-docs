{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Convergence study (spatial & temporal refinement)\n\nThis example shows one possible implementation of how to do a convergence study\nwith spatial and temporal refinement. For this, a simple model using a time\ndependent heat source on one side and constant temperature on the opposite side\nwas set up. The heat source is generated with the\n:py:mod:`ogstools.physics.nuclearwasteheat` model.\n\nHere is some theoretical background for the topic of grid convergence:\n\n[Nasa convergence reference](https://www.grc.nasa.gov/www/wind/valid/tutorial/spatconv.html)\n\n[More comprehensive reference](https://curiosityfluids.com/2016/09/09/establishing-grid-convergence/)\n\nAt least three meshes from simulations of increasing refinement are\nrequired for the convergence study. The third finest mesh is chosen per default\nas the topology to evaluate the results on.\n\nThe results to analyze are generated on the fly with the following code. If you\nare only interested in the convergence study, please skip to\n`Temperature convergence at maximum heat production (t=30 yrs)`_.\n\nFirst, the required packages are imported and a temporary output directory is\ncreated:\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from pathlib import Path\nfrom shutil import rmtree\nfrom tempfile import mkdtemp\n\nimport matplotlib.pyplot as plt\nimport numpy as np\nfrom IPython.display import HTML\nfrom ogs6py import ogs\nfrom scipy.constants import Julian_year as sec_per_yr\n\nfrom ogstools import meshlib, msh2vtu, physics, propertylib, studies, workflow\n\ntemp_dir = Path(mkdtemp(prefix=\"nuclear_decay\"))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's run the different simulations with increasingly fine spatial and\ntemporal discretization via ogs6py. The mesh and its boundaries are generated\neasily via gmsh and :py:mod:`ogstools.msh2vtu`. First some definitions:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "n_refinements = 4\ntime_step_sizes = [30.0 / (2.0**r) for r in range(n_refinements)]\nprefix = \"stepsize_{0}\"\nsim_results = []\nmsh_path = temp_dir / \"rect.msh\"\nscript_path = Path(studies.convergence.examples.nuclear_decay_bc).parent\nprj_path = studies.convergence.examples.nuclear_decay_prj\nogs_args = f\"-m {temp_dir} -o {temp_dir} -s {script_path}\"\nedge_cells = [5 * 2**i for i in range(n_refinements)]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Now the actual simulations:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "for dt, n_cells in zip(time_step_sizes, edge_cells):\n    meshlib.rect(lengths=100.0, n_edge_cells=[n_cells, 1], out_name=msh_path)\n    _ = msh2vtu.msh2vtu(msh_path, output_path=temp_dir, log_level=\"ERROR\")\n\n    model = ogs.OGS(PROJECT_FILE=temp_dir / \"default.prj\", INPUT_FILE=prj_path)\n    model.replace_text(str(dt * sec_per_yr), \".//delta_t\")\n    model.replace_text(prefix.format(dt), \".//prefix\")\n    model.write_input()\n    model.run_model(write_logs=False, args=ogs_args)\n    sim_results += [temp_dir / (prefix.format(dt) + \"_rect_domain.xdmf\")]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's extract the temperature evolution and the applied heat via vtuIO and\nplot both:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "time = np.append(0.0, np.geomspace(1.0, 180.0, num=100))\nrepo = physics.nuclearwasteheat.repo_2020_conservative\nheat = repo.heat(time, time_unit=\"yrs\", power_unit=\"kW\")\nfig, (ax1, ax2) = plt.subplots(figsize=(8, 8), nrows=2, sharex=True)\nax2.plot(time, heat, lw=2, label=\"reference\", color=\"k\")\n\nfor sim_result, dt in zip(sim_results, time_step_sizes):\n    mesh_series = meshlib.MeshSeries(sim_result)\n    results = {\"heat_flux\": [], \"temperature\": []}\n    for ts in mesh_series.timesteps:\n        mesh = mesh_series.read(ts)\n        results[\"temperature\"] += [np.max(mesh.point_data[\"temperature\"])]\n    max_T = propertylib.presets.temperature(results[\"temperature\"]).magnitude\n    # times 2 due to symmetry, area of repo, to kW\n    results[\"heat_flux\"] += [np.max(mesh.point_data[\"heat_flux\"][:, 0])]\n    tv = np.asarray(mesh_series.timevalues) / sec_per_yr\n    ax1.plot(tv, max_T, lw=1.5, label=f\"{dt=}\")\n    edges = np.append(0, tv)\n    mean_t = 0.5 * (edges[1:] + edges[:-1])\n    applied_heat = repo.heat(mean_t, time_unit=\"yrs\", power_unit=\"kW\")\n    ax2.stairs(applied_heat, edges, lw=1.5, label=f\"{dt=}\", baseline=None)\nax2.set_xlabel(\"time / yrs\")\nax1.set_ylabel(\"max T / \u00b0C\")\nax2.set_ylabel(\"heat / kW\")\nax1.legend()\nax2.legend()\nfig.show()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Temperature convergence at maximum heat production (t=30 yrs)\n\nThe grid convergence at this timepoint deviates significantly from 1,\nmeaning the convergence is suboptimal (at least on the left boundary where the\nheating happens). The chosen timesteps are still to coarse to reach an\nasymptotic range of convergence. The model behavior at this early part of the\nsimulation is still very dynamic and needs finer timesteps to be captured with\ngreat accuracy. Nevertheless, the maximum temperature converges quadratically,\nas expected.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "report_name = temp_dir / \"report.ipynb\"\nstudies.convergence.run_convergence_study(\n    output_name=report_name,\n    mesh_paths=sim_results,\n    timevalue=30 * sec_per_yr,\n    property_name=\"temperature\",\n    refinement_ratio=2.0,\n)\nHTML(workflow.jupyter_to_html(report_name, show_input=False))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Temperature convergence at maximum temperature (t=150 yrs)\n\nThe temperature convergence at this timevalue is much closer to 1, indicating\na better convergence behaviour, which is due to the temperature gradient now\nchanging only slowly. Convergence order is again quadratic.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "studies.convergence.run_convergence_study(\n    output_name=report_name,\n    mesh_paths=sim_results,\n    timevalue=150 * sec_per_yr,\n    property_name=\"temperature\",\n    refinement_ratio=2.0,\n)\nHTML(workflow.jupyter_to_html(report_name, show_input=False))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Convergence evolution over all timesteps\n\nWe can also run the convergence evaluation on all timesteps and look at the\nrelative errors (between finest discretization and Richardson extrapolation)\nand the convergence order over time to get a better picture of the transient\nmodel behavior.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh_series = [meshlib.MeshSeries(sim_result) for sim_result in sim_results]\nevolution_metrics = studies.convergence.convergence_metrics_evolution(\n    mesh_series, propertylib.presets.temperature, units=[\"s\", \"yrs\"]\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Looking at the relative errors, we see a higher error right at the beginning.\nThis is likely due to the more dynamic behavior at the beginning.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = studies.convergence.plot_convergence_error_evolution(evolution_metrics)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "A look at the convergence order evolution shows almost quadratic convergence\nover the whole timeframe. For the maximum temperature we even get better than\nquadratic behavior, which is coincidentally and most likely model dependent.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = studies.convergence.plot_convergence_order_evolution(evolution_metrics)"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.16"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}