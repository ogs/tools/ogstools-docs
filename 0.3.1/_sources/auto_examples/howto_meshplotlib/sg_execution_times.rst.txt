
:orphan:

.. _sphx_glr_auto_examples_howto_meshplotlib_sg_execution_times:


Computation times
=================
**00:54.946** total execution time for 9 files **from auto_examples/howto_meshplotlib**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_meshplotlib_3d.py` (``plot_meshplotlib_3d.py``)
     - 00:14.527
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_animation.py` (``plot_animation.py``)
     - 00:13.534
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_meshplotlib_2d.py` (``plot_meshplotlib_2d.py``)
     - 00:08.276
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_solid_mechanics.py` (``plot_solid_mechanics.py``)
     - 00:06.357
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_xy_labels_with_shared_axes.py` (``plot_xy_labels_with_shared_axes.py``)
     - 00:03.536
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_aspect_ratios.py` (``plot_aspect_ratios.py``)
     - 00:02.906
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_with_custom_fig_ax.py` (``plot_with_custom_fig_ax.py``)
     - 00:02.654
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_special.py` (``plot_special.py``)
     - 00:01.804
     - 0.0
   * - :ref:`sphx_glr_auto_examples_howto_meshplotlib_plot_observation_points.py` (``plot_observation_points.py``)
     - 00:01.352
     - 0.0
