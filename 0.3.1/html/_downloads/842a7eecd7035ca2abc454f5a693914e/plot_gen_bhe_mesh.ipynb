{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Creating a BHE mesh (Borehole Heat Exchanger)\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This example demonstrates how to use :py:mod:`ogstools.meshlib.gmsh_meshing.gen_bhe_mesh` to create\na Borehole Heat Exchanger (BHE) mesh.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from pathlib import Path\nfrom tempfile import mkdtemp\n\nimport pyvista as pv\nfrom pyvista.plotting import Plotter\n\nfrom ogstools.meshlib.gmsh_meshing import BHE, Groundwater, gen_bhe_mesh"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 0. Introduction\nThis example shows the general usage of :py:mod:`ogstools.meshlib.gmsh_meshing.gen_bhe_mesh` and how some of\nthe parameters will effect the mesh. This section demonstrates the mesh\ngeneration with only three soil layers, groundwater flow in one layer\nand three BHE's. However, this tool provides multiple soil layers,\ngroundwater flow in multiple layers and multiple BHE's. The mesh sizes\nprovides good initial values for the most Heat-Transport-BHE simulations\nin OGS. They can also be set by the user, to customize the mesh.\nFeel free to try it out!\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 1. Create a simple prism mesh\nGenerate a customizable prism BHE mesh:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "tmp_dir = Path(mkdtemp())\nvtu_file = tmp_dir / \"bhe_prism.vtu\"\nbhe_meshes = gen_bhe_mesh(\n    length=150,\n    width=100,\n    layer=[50, 50, 50],\n    groundwater=Groundwater(\n        begin=-30, isolation_layer_id=1, flow_direction=\"+x\"\n    ),\n    BHE_Array=[\n        BHE(x=50, y=40, z_begin=-1, z_end=-60, borehole_radius=0.076),\n        BHE(x=50, y=50, z_begin=-1, z_end=-60, borehole_radius=0.076),\n        BHE(x=50, y=60, z_begin=-1, z_end=-60, borehole_radius=0.076),\n    ],\n    meshing_type=\"prism\",\n    out_name=vtu_file,\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Load the domain mesh and all submeshes as well as extract the BHE lines:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh = pv.read(tmp_dir / bhe_meshes[0])\ntop_mesh = pv.read(tmp_dir / bhe_meshes[1])\nbottom_mesh = pv.read(tmp_dir / bhe_meshes[2])\ngw_mesh = pv.read(tmp_dir / bhe_meshes[3])\nbhe_line = mesh.extract_cells_by_type(pv.CellType.LINE)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Visualize the mesh:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "top_mesh = top_mesh.translate((0, 0, 10), inplace=False)\nbottom_mesh = bottom_mesh.translate((0, 0, -10), inplace=False)\ngw_mesh = gw_mesh.translate((-10, 0, 0), inplace=False)\n\np = Plotter()\np.add_mesh(mesh, style=\"wireframe\", color=\"grey\")\np.add_mesh(\n    mesh.clip(\"x\", bhe_line.center, crinkle=True),\n    show_edges=True,\n    scalars=\"MaterialIDs\",\n    cmap=\"Accent\",\n    categories=True,\n    scalar_bar_args={\"vertical\": True, \"n_labels\": 2, \"fmt\": \"%.0f\"},\n)\np.add_mesh(bhe_line, color=\"r\", line_width=3)\np.add_mesh(top_mesh, show_edges=True)\np.add_mesh(bottom_mesh, show_edges=True)\np.add_mesh(gw_mesh, show_edges=True)\np.add_axes()\np.show()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 1. Create a simple structured mesh\nGenerate a customizable structured BHE mesh:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "tmp_dir = Path(mkdtemp())\nvtu_file = tmp_dir / \"bhe_structured.vtu\"\nbhe_meshes = gen_bhe_mesh(\n    length=150,\n    width=100,\n    layer=[50, 50, 50],\n    groundwater=Groundwater(\n        begin=-30, isolation_layer_id=1, flow_direction=\"+x\"\n    ),\n    BHE_Array=[\n        BHE(x=50, y=40, z_begin=-1, z_end=-60, borehole_radius=0.076),\n        BHE(x=50, y=50, z_begin=-1, z_end=-60, borehole_radius=0.076),\n        BHE(x=50, y=60, z_begin=-1, z_end=-60, borehole_radius=0.076),\n    ],\n    meshing_type=\"structured\",\n    out_name=vtu_file,\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Load the domain mesh and all submeshes as well as extract the BHE lines:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh = pv.read(tmp_dir / bhe_meshes[0])\ntop_mesh = pv.read(tmp_dir / bhe_meshes[1])\nbottom_mesh = pv.read(tmp_dir / bhe_meshes[2])\ngw_mesh = pv.read(tmp_dir / bhe_meshes[3])\nbhe_line = mesh.extract_cells_by_type(pv.CellType.LINE)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Visualize the mesh:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "top_mesh = top_mesh.translate((0, 0, 10), inplace=False)\nbottom_mesh = bottom_mesh.translate((0, 0, -10), inplace=False)\ngw_mesh = gw_mesh.translate((-10, 0, 0), inplace=False)\n\np = Plotter()\np.add_mesh(mesh, style=\"wireframe\", color=\"grey\")\np.add_mesh(\n    mesh.clip(\"x\", bhe_line.center, crinkle=True),\n    show_edges=True,\n    scalars=\"MaterialIDs\",\n    cmap=\"Accent\",\n    categories=True,\n    scalar_bar_args={\"vertical\": True, \"n_labels\": 2, \"fmt\": \"%.0f\"},\n)\np.add_mesh(bhe_line, color=\"r\", line_width=3)\np.add_mesh(top_mesh, show_edges=True)\np.add_mesh(bottom_mesh, show_edges=True)\np.add_mesh(gw_mesh, show_edges=True)\np.add_axes()\np.show()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 2. Create an advanced structured mesh\nGenerate a customizable structured BHE mesh with advanced mesh\nsizing options (using gmsh). To understand the specific\nbehaviour of every mesh parameter, test each one after another.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "vtu_file = tmp_dir / \"bhe_structured_advanced.vtu\"\nbhe_meshes = gen_bhe_mesh(\n    length=150,\n    width=100,\n    layer=[50, 50, 50],\n    groundwater=Groundwater(-30, 1, \"+x\"),\n    BHE_Array=[\n        BHE(50, 40, -1, -60, 0.076),\n        BHE(50, 50, -1, -60, 0.076),\n        BHE(50, 60, -1, -60, 0.076),\n    ],\n    meshing_type=\"structured\",\n    target_z_size_coarse=10,  # default value 7.5\n    target_z_size_fine=2,  # default value 1.5\n    n_refinement_layers=1,  # default value 2\n    dist_box_x=15,  # default value 10\n    propagation=1.2,  # default value 1.1\n    inner_mesh_size=8,  # default value 5\n    outer_mesh_size=12,  # default value 10\n    out_name=vtu_file,\n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Load the domain mesh and all submeshes as well as extract the BHE lines:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh = pv.read(tmp_dir / bhe_meshes[0])\ntop_mesh = pv.read(tmp_dir / bhe_meshes[1])\nbottom_mesh = pv.read(tmp_dir / bhe_meshes[2])\ngw_mesh = pv.read(tmp_dir / bhe_meshes[3])\nbhe_line = mesh.extract_cells_by_type(pv.CellType.LINE)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Visualize the mesh:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "top_mesh = top_mesh.translate((0, 0, 10), inplace=False)\nbottom_mesh = bottom_mesh.translate((0, 0, -10), inplace=False)\ngw_mesh = gw_mesh.translate((-10, 0, 0), inplace=False)\n\np = Plotter()\np.add_mesh(mesh, style=\"wireframe\", color=\"grey\")\np.add_mesh(\n    mesh.clip(\"x\", bhe_line.center, crinkle=True),\n    show_edges=True,\n    scalars=\"MaterialIDs\",\n    cmap=\"Accent\",\n    categories=True,\n    scalar_bar_args={\"vertical\": True, \"n_labels\": 2, \"fmt\": \"%.0f\"},\n)\np.add_mesh(bhe_line, color=\"r\", line_width=3)\np.add_mesh(top_mesh, show_edges=True)\np.add_mesh(bottom_mesh, show_edges=True)\np.add_mesh(gw_mesh, show_edges=True)\np.add_axes()\np.show()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}