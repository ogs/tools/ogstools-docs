

.. _sphx_glr_auto_examples_howto_propertylib:

Features of propertylib
=======================

.. sectionauthor:: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)

The following jupyter notebook shows some example usage of the propertylib submodule.



.. raw:: html

    <div class="sphx-glr-thumbnails">

.. thumbnail-parent-div-open

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip=":pyogstools.propertylib provides classes (Scalar, Vector, Matrix) which encapsulate unit handli...">

.. only:: html

  .. image:: /auto_examples/howto_propertylib/images/thumb/sphx_glr_plot_propertylib_thumb.png
    :alt:

  :ref:`sphx_glr_auto_examples_howto_propertylib_plot_propertylib.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Property Presets</div>
    </div>


.. thumbnail-parent-div-close

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /auto_examples/howto_propertylib/plot_propertylib

