{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Differences between meshes\n\n.. sectionauthor:: Feliks Kiszkurno (Helmholtz Centre for Environmental Research GmbH - UFZ)\n\nThis example explains how to use functions from meshlib to calculate differences\nbetween meshes.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from ogstools import examples\nfrom ogstools.meshlib import difference, difference_matrix, difference_pairwise\nfrom ogstools.propertylib import properties\n\nmesh_property = properties.temperature"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 0. Introduction\nThis example shows how to calculate differences between meshes.\nIt is possible to calculate the difference between multiple meshes at the same\ntime. Multiple meshes can be provided either as list or Numpy arrays.\n4 ways of calculating the difference are presented here.\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 1. Difference between two meshes\nThe simplest case is calculating the difference between two meshes. For this\nexample, we read two different timesteps from a MeshSeries. It is not required\nthat they belong to the same MeshSeries object. As long, as the meshes share\nthe same topology and contain the mesh_property of interest, the difference\nwill work fine.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh_series = examples.load_meshseries_THM_2D_PVD()\nmesh1 = mesh_series.read(0)\nmesh2 = mesh_series.read(-1)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The following call will return a mesh containing the difference of the\nmesh_property between the two provided meshes:\n\n\\begin{align}\\text{Mesh}_1 - \\text{Mesh}_2\\end{align}\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh_diff = difference(mesh1, mesh2, mesh_property)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This returned object will be a PyVista UnstructuredGrid object:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Type of mesh_diff: {type(mesh_diff)}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 2. Pairwise difference\nIn order to calculate pairwise differences, two lists or arrays of equal\nlength have to be provided as the input. They can contain an arbitrary number\nof different meshes, as long as their length is equal.\n\nConsider the two following lists:\n\n\\begin{align}\\text{List}_{1} = \\begin{bmatrix} A_{1} & B_{1} & C_{1}\\\\ \\end{bmatrix}\\end{align}\n\nand\n\n\\begin{align}\\text{List}_{2} = \\begin{bmatrix} A_{2} & B_{2} & C_{2}\\\\ \\end{bmatrix}\\end{align}\n\nThe output array will contain following differences between meshes:\n\n\\begin{align}\\begin{bmatrix} A_{1}-A_{2} & B_{1}-B_{2} & C_{1}-C_{2}\\\\ \\end{bmatrix}\\end{align}\n\nand will have the same shape as the input lists. As in the example below:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "meshes_1 = [mesh1] * 3\nmeshes_2 = [mesh2] * 3\n\nmesh_diff_pair_wise = difference_pairwise(meshes_1, meshes_2, mesh_property)"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Length of mesh_list1: {len(meshes_1)}\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Length of mesh_list2: {len(meshes_2)}\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Shape of mesh_diff_pair_wise: {mesh_diff_pair_wise.shape}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 3. Matrix difference - one array\nIf only one list or array is provided, the differences between every possible\npair of objects in the array will be returned.\n\nConsider following list:\n\n\\begin{align}\\text{List} = \\begin{bmatrix} A & B & C\\\\ \\end{bmatrix}\\end{align}\n\nThe output array will contain following differences between meshes:\n\n\\begin{align}\\begin{bmatrix} A-A & B-A & C-A\\\\ A-B & B-B & C-B \\\\ A-C & B-C & C-C \\\\ \\end{bmatrix}\\end{align}\n\nand will have shape of (len(mesh_list), len(mesh_list)). As in the following\nexample:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh_list = [mesh1, mesh2, mesh1, mesh2]\n\nmesh_diff_matrix = difference_matrix(mesh_list, mesh_property=mesh_property)"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Length of mesh_list1: {len(mesh_list)}\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Shape of mesh_list1: {mesh_diff_matrix.shape}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## 4. Matrix difference - two arrays of different length\nUnlike difference_pairwise(), difference_matrix() can accept two lists/arrays\nof different lengths. As in Section 3, the differences between all possible\npairs of meshes in both lists/arrays will be calculated.\n\nConsider following lists:\n\n\\begin{align}\\text{List}_1 = \\begin{bmatrix} A_1 & B_1 & C_1\\\\ \\end{bmatrix}\\end{align}\n\n\\begin{align}\\text{List}_2 = \\begin{bmatrix} A_2 & B_2 \\\\ \\end{bmatrix}\\end{align}\n\nThe output array will contain following differences between meshes:\n\n\\begin{align}\\begin{bmatrix} A_1-A_2 & A_1-B_2 \\\\ B_1-A_2 & B_1-B_2 \\\\ C_1-A_2 & C_1-B_2 \\\\ \\end{bmatrix}\\end{align}\n\nand will have a shape of (len(mesh_list_matrix_1), len(mesh_list_matrix_2)).\nAs in the following example:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "mesh_list_matrix_1 = [mesh1, mesh2, mesh1]\nmesh_list_matrix_2 = [mesh2, mesh1]\n\nmesh_diff_matrix = difference_matrix(\n    mesh_list_matrix_1, mesh_list_matrix_2, mesh_property\n)"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Length of mesh_list_matrix_1: {len(mesh_list_matrix_1)}\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Length of mesh_list_matrix_2: {len(mesh_list_matrix_2)}\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print(f\"Shape of mesh_diff_matrix: {mesh_diff_matrix.shape}\")"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.18"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}